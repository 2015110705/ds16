/*
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* linked list */
typedef struct node* tListNodePointer;
typedef struct node {
	char				data;
	tListNodePointer	link;
} tListNode;

tListNodePointer	head = NULL;

tListNodePointer	initList(const char* str);
tListNodePointer	copy(tListNodePointer head);
tListNodePointer	makeNewNode(const char data);
tListNodePointer	invert(tListNodePointer lead);
void				printList(const tListNodePointer head);
int					isPalindrome(tListNodePointer head);

int main()
{
	char				string[20];
	tListNodePointer	list;

	/* input string from keyboard */
	printf("Enter the string to check palindrome : ");
	scanf("%s", string);

	/* initialize the list */
	list = initList(string);

	/* check that string is palindrome */
	if (isPalindrome(list) == 1) {
		printf("%s is palindrome \n", string);
	}
	else {
		printf("%s is not palindrome \n", string);
	}

	return 0;
}

/**
 * make linked list from string str
 * @param	: string str
 * @return	: list's first node
 */
tListNodePointer initList(const char* str)
{
	int					i;
	tListNodePointer	head = NULL;
	tListNodePointer	iter = NULL;

	if (strlen(str) == 0) {
		return NULL;
	}

	head = makeNewNode(str[0]);
	iter = head;

	for (i = 1; i < strlen(str); i++) {
		iter->link = makeNewNode(str[i]);
		iter = iter->link;
	}
	
	return head;
}

/**
 * make linked list's clone
 * @param	: list's first node
 * @return	: clone's first node
 */
tListNodePointer copy(tListNodePointer head)
{
	tListNodePointer headPointer	= NULL;
	tListNodePointer iter			= NULL;

	if (head == NULL) {
		return NULL;
	}
	
	headPointer = makeNewNode(head->data);
	iter = headPointer;

	while (head->link != NULL) {
		iter->link = makeNewNode(head->link->data);
		iter = iter->link;
		head = head->link;
	}

	return headPointer;
}

/**
 * make new node
 * @param	: node's data
 * @return	: new node
 */
tListNodePointer makeNewNode(const char data)
{
	tListNodePointer newNode = (tListNodePointer)malloc(sizeof(tListNode));
	
	newNode->link = NULL;
	newNode->data = data;
	
	return newNode;
}

/**
 * invert linked list
 * @param	: lead node
 * @return	: invert list's first node
 */
tListNodePointer invert(tListNodePointer lead)
{
	tListNodePointer middle		= NULL;
	tListNodePointer trail		= NULL;

	while (lead) {
		trail = middle;
		middle = lead;
		lead = lead->link;
		middle->link = trail;
	}
	
	return middle;
}

/**
 * print linked list
 * @param	: linked list's first node
 */
void printList(const tListNodePointer head)
{
	tListNodePointer temp = head;

	while (temp) {
		printf("%c ", temp->data);
		temp = temp->link;
	}
	printf("\n");
}

/**
 * check that string is palindrome from linked list
 * @param	: linked list's first node
 * @return	: if string is palindrome then return 1, if not then return 0
 */
int	isPalindrome(tListNodePointer head)
{
	tListNodePointer invertList = copy(head);
	tListNodePointer iter = NULL;

	invertList = invert(invertList);
	iter = invertList;

	/* print list and invert list */
	printf("initial linked list : ");
	printList(head);
	printf("inverted linked list : ");
	printList(invertList);

	/* check that list and invert list match each other */
	while (iter != NULL) {
		if (iter->data != head->data) {
			return 0;
		}
		iter = iter->link;
		head = head->link;
	}

	return 1;
}

