/*
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 100
#define INF_NUM 10000

/* min - winner tree */
int nums[MAX_SIZE + 1][11] = { 0 };		//keys to sort
int winTree[2 * MAX_SIZE] = { NULL };	//winner tree
int sorted[MAX_SIZE * 10 + 1] = { 0 };	//sorted result
int sortedIdx[MAX_SIZE];				//sorted index

int initWinner(int cur, int k, int winTree[]);
void adjustWinner(int min, int k, int winTree[]);
void inorder(int root, int k, int winTree[]);

int main()
{
	int seed;
	int k;
	
	int i, j;

	printf("<<<<<<<<<<<<<<<<<<<<<<<< sorting with winner tree >>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n");
	
	/* input seed and k */
	printf("the number of keys (8, 16, or 32 as a power of 2 )  >> ");
	scanf("%d", &k);
	printf("random numbers to use as key values (1 ~ 100) \n");
	printf("seed >> ");
	scanf("%d", &seed);
	srand(seed);

	/* make 2 dimension array (k * 10) */
	/*	save number to array from k to k + 9 
		random number range : 1 ~ 100 (can duplicate) */
	for (i = 1; i <= k; i++) {
		printf("%2d-th records : \n", i);
		
		nums[1][i] = rand() % 100 + 1;	
		for (j = 2; j <= 10; j++) {
			nums[j][i] = nums[1][i] + j - 1;
		}

		for (j = 1; j <= 10; j++) {
			printf("%2d ", nums[j][i]);
		}
		printf("\n");
	}
	printf("\n");

	/* initialize min-winner tree */
	printf("initialization of min-winner tree \n\n");
	for (i = k; i < k * 2; i++) {
		winTree[i] = i - k + 1;
	}
	for (i = 1; i <= k; i++) {
		sortedIdx[i] = 1;
	}
	initWinner(1, k, winTree);
	

	/* inorder traversal for min-winner tree */
	printf("inorder traversal for min-winner tree\n\n\n");

	/* sorting with min-winner tree */
	printf("sorting with min-winner tree...\n\n");
	adjustWinner(1, k, winTree);

	/* print sorting result */
	printf("sorted result\n");
	for (i = 1; i <= k * 10; i++) {
		printf("%3d ", sorted[i]);
		if (i % k == 0) {
			printf("\n");
		}
	}

	return 0;
}

int initWinner(int cur, int k, int winTree[]) 
{
	if (cur < k * 2) {
		initWinner(cur * 2, k, winTree);
		initWinner(cur * 2 + 1, k, winTree);

		if (cur % 2 == 1) {
			if (nums[sortedIdx[winTree[cur]]][winTree[cur]] <=
				nums[sortedIdx[winTree[cur - 1]]][winTree[cur - 1]]) {
				winTree[cur / 2] = winTree[cur];
			}
			else {
				winTree[cur / 2] = winTree[cur - 1];
			}
		}
	}
}

void adjustWinner(int min, int k, int winTree[])
{
	int i;

	for (i = 1; i <= k * 10; i++) {
		sorted[i] = nums[sortedIdx[winTree[1]]][winTree[1]];
		nums[sortedIdx[winTree[1]]][winTree[1]] = 999;
		if (sortedIdx[winTree[1]] < 10) {
			sortedIdx[winTree[1]]++;
		}
		initWinner(1, k, winTree);
	}
}

void inorder(int root, int k, int winTree[])
{
	inorder(root * 2, k, winTree);

	inorder(root * 2 + 1, k, winTree);
}