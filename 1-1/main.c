/*
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#include <stdio.h>
#include <string.h>

int isPalindrome(const char* str);

int main()
{
	char string[20];

	/* input string from keyboard */
	printf("Enter the string to check palindrome : ");
	scanf("%s", string);

	/* check that string is palindrome */
	if (isPalindrome(string) == 1) {
		printf("%s is palindrome \n", string);
	}
	else {
		printf("%s is not palindrome \n", string);
	}

	return 0;
}

/**
 * check that string is palindrome
 * @param	: (const)string str
 * @return	: if string is palindrome then return 1, if not then return 0
 */
int isPalindrome(const char* str)
{
	int i;

	for (i = 0; i <= strlen(str) / 2; i++) {
		if (str[i] != str[strlen(str) - 1 - i]) {
			return 0;
		}
	}

	return 1;
}